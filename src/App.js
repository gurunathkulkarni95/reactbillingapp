import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import { routes } from "./pages/Router/router";
export default class App extends Component {
  render() {
    return (
      <React.Fragment>
        <Switch>
          {routes.map(route => {
            return <Route exact {...route} key={route.path} />;
          })}
        </Switch>
      </React.Fragment>
    );
  }
}
