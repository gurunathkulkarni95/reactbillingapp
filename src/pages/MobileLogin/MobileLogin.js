import React, { Component } from "react";
import "./MobileLogin.css";
export default class MobileLogin extends Component {


  submitButton = () =>{
    this.props.history.push("/BillingScreen");
  }
  render() {
    return (
      <div class="box">
      <h2>Login</h2>
      <form action="">
          <div class="inputBox">
              <input type="text" name="" required=""/>
              <label for="">Username</label>
          </div>
          <div class="inputBox">
              <input type="password" name="" required=""/>
              <label for="">Password</label>
          </div>
          <input type="submit" name="" value="Submit" onClick={this.submitButton}/>
      </form>
  </div>
    );
  }
}
