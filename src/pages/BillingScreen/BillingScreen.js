import React, { Component } from "react";
import "./BillingScreen.css";
import Header from "../../Components/Header/Header";
import { Table } from "react-bootstrap";
import Select from "react-select";
import { throwStatement } from "@babel/types";
const optionscategory = [
  {
    value: "chocolate",
    label: "Chocolate",
    id: 1,
    products: [
      {
        id: 1,
        product_name: "Dairy Milk",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 2,
        product_name: "kitkat",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 3,
        product_name: "Dairy milk 10",
        price: "10",
        selling_price: "12",
        tax_rate: "0"
      },
      {
        id: 4,
        product_name: "Dairy Milk 20",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 5,
        product_name: "Dairy Milk 30",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 6,
        product_name: "Dairy Milk 40",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 7,
        product_name: "kitkat 10",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      },
      {
        id: 18,
        product_name: "munch",
        price: "5",
        selling_price: "7",
        tax_rate: "0"
      }
    ]
  },
  { value: "strawberry", label: "Strawberry", id: 2 },
  { value: "vanilla", label: "Vanilla", id: 3 }
];
export default class BillingScreen extends Component {
  constructor() {
    super();
    this.state = {
      category: {},
      product: [],
      product_details: {},
      cart: []
    };
  }

  componentDidMount() {
    this.setState({ category: optionscategory });
  }

  handleChange = e => {
    console.log(e.products);
    if (e.products === undefined) {
      alert("no products available");
    } else {
      this.setState({ product: e.products }, () => {
        this.state.product.map(
          data => ((data.label = data.product_name), (data.value = data.price))
        );

        console.log(this.state.product);
      });
    }
  };

  handleProduct = e => {
    console.log("cartItems", e);
    let check = this.state.cart.filter(data => {
      return data.id === e.id;
    });
    console.log("checkLength", check.length);
    if (check.length === 0) {
      this.state.cart.push(e);
      this.setState({ product_details: e }, () => {
        console.log(this.state);
      });
    } else {
      alert("Item is on cart");
    }
    console.log("check", check);
  };

  render() {
    const { category, product, cart } = this.state;
    let tableRows = null;
    console.log("cart", cart);
    if (cart.length > 0) {
      tableRows = (
          <tr >
          {this.state.cart.map((data, index) => {
            return (
                <React.Fragment>
                <td key={index}>{data.product_name}</td>
                <td key={index}>{data.price}</td> 
                <td key={index}>
                </td>
                </React.Fragment>
            );
          })}
          </tr>
      );
    }
    return (
      <React.Fragment>
        <Header />
        <div className="row">
          <div className="col-md-6 mt-2 pr-4 pl-4">
            <Select
              options={category}
              name="category"
              onChange={this.handleChange}
            />
          </div>
          <div className="col-md-6 mt-2 pr-4  pl-4">
            <Select options={product} onChange={this.handleProduct} />
          </div>
        </div>
        <div className="tableCart">
          <h2 className="tableTitle">Cart Items</h2>
          <Table bordered>
            <thead>
              <tr>
                <th>product_name</th>
                <th>product_price</th>
                <th>product_QTY</th>
              </tr>
            </thead>
            <tbody>{tableRows}</tbody>
          </Table>
        </div>
      </React.Fragment>
    );
  }
}
