import MobileLogin from "../MobileLogin/MobileLogin";
import MobileOtp from "../MobileOTP/MobileOtp";
import BillingScreen from "../BillingScreen/BillingScreen";
import Product from "../Inventory_Management/Product/Product";
import Category from "../Inventory_Management/Category/Category";

export const routes = [
    {
        path:"/",
        component:MobileLogin
    },
    {
        path:"/mobileOtp",
        component:MobileOtp
    },
    {
        path:"/BillingScreen",
        component:BillingScreen
    },
    {
        path:"/Product",
        component:Product
    },
    {
        path:"/category",
        component:Category
    }
]