import React, { Component } from 'react'
import './header.css';
var deferredPrompt;
export default class Header extends Component {
  addToHomeScreen = () =>{
    console.log("share", navigator)
    if (navigator.share) {
      navigator.share({
        title: 'web.dev',
        text: 'Check out pwa .',
        url: window.location.href,
      })
        .then(() => console.log('Successful share'))
        .catch((error) => console.log('Error sharing', error));
    }
  }
  render() {
    return (
      <header class="header-fixed">

      <div class="header-limiter">
    
        <h1><a href="#">Company<span>logo</span></a></h1>
    
        <nav>
          <a href="#" class="selected">Home</a>
          <a onClick={this.addToHomeScreen} >share</a>
          <a href="#">Billing</a>
          <a href="#">Customer</a>
          <a href="#">profile</a>
          <a href="#">Inventory</a>
        </nav>
    
      </div>
    
    </header>
    )
  }
}
