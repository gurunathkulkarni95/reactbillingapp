import fetch from "isomorphic-fetch";

// This method performs get API request. It expects the url,header
export const get = ({ url, header = {} }) => {
  return fetch(url, header)
    .then(response => {
      if (response.status >= 400 && !checkURL) {
        throw new Error(`Bad response from server${url}`);
      }
      if (response.status < 400 && !checkURL) {
        return response.json();
      }
      return response.json();
    })
    .then(response => {
      return response;
    });
};

//This method performs post API request . It expects the url , header object(request paramters)
export const post = async ({ url, header = {}, obj }) => {
  return fetch(url, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      ...header.headers
    },
    body: JSON.stringify(obj)
  })
    .then(response => {
      if (response.status >= 400 && !checkURL) {
        throw new Error("Bad response from server");
      }
      if (response.status < 400 && !checkURL) {
        if (htmlResponse) return response.text();
        return response.json();
      }
      return response.json();
    })
    .then(data => {
      return data;
    });
};
